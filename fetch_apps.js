'use strict';

// DEBUG=google-play-scraper node fetch_apps.js

var gplay = require('google-play-scraper');
var csv = require("fast-csv");
var fs = require('fs');
var path = require('path');
var osmosis = require('osmosis');
var mkdirp = require('mkdirp');

var googlePlayCountries = [];

const company = 'Samsung Electronics Co.,  Ltd.'; 

/* 
 Google Inc.
 Microsoft Corporation 
 Intel Corporation
 McAfee (Intel Security)
 Apple Inc.
 NVIDIA
 Lyft, Inc.
 Uber Technologies, Inc.
 Facebook
 United Nations
 Amazon Mobile LLC
 Alibaba.com Hong Kong Limited
 Samsung Electronics Co.,  Ltd.
 HTC Corporation
 Motorola Mobility LLC.
 Huawei Technologies Co., Ltd.
 Huawei Internet Service
 OnePlus Ltd.
 Xiaomi Technology
 Xiaomi Inc.
 Lenovo Inc.
 Lenovo Group
 Lenovo IT Security
 Lenovo Inc. (DCG)
 Qualcomm Innovation Center, Inc.
 Qualcomm Connected Experiences, Inc.
 Urbandroid Team
 LG Electronics, Inc.
 LG Electronics RUSSIA R&D Lab
 LG Learning Canada
 LG Electronics Accessory
 LG Electronics India Pvt Ltd.
 TCL Communication
 TCL-CONNECTED
 Customer Mobile Alcatel
 mie-alcatel.support
 TCL Communication LIMITED
 ZTE Corp
 ZTE USA
 ZTE MBB
 中兴通讯
*/

osmosis.get('https://support.google.com/googleplay/android-developer/table/3541286?hl=en')
                .config('user_agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36')
                .find('//*/div[@class="dyn-table"]/table/tbody/tr/td[1]/text()')
                .set('country')
                .data(function(data) {
                    //console.log(data['country']);
                    const country = data['country'];
                    if (country !== 'Rest of the World')
                        googlePlayCountries.push(country);
                })
                .done(function () {
                    //console.log(googlePlayCountries);
                    //googlePlayCountries = ['Germany', 'France'];
                    googlePlayCountries = googlePlayCountries.sort();
                    var countries = new Map();

                    fs.createReadStream("countries.csv")
                    .pipe(csv())
                    .on("data", function(data){
                        //console.log(data);
                        countries.set(data[0], data[1]);
                    })
                    .on("end", function() {
                       
                        var completedCountries = [];

                        function getNextCountry() {
                            const countryName = googlePlayCountries.shift();
                            if (countryName === undefined)
                                return null;

                            const countryCode = countries.get(countryName).toLowerCase();
                            if (countryCode === undefined) {
                                console.error("Cannot find country code for Google Play country:", countryName);
                                process.exit();
                            }

                            return { name: countryName, code: countryCode};
                        }

                        function shouldQuit() {
                            console.log(completedCountries.length, googlePlayCountries.length);
                            return completedCountries.length == googlePlayCountries.length;
                        }

                        function fetchNextCountry(dontSleep) {
                            const nextCountry = getNextCountry();
                            if (nextCountry !== null) {
                                if (typeof(dontSleep) === 'undefined') {
                                    setTimeout(getApps, 5000, nextCountry);
                                }
                                else {
                                    getApps(nextCountry);
                                }
                                return 42;
                            } else  {
                                return null;
                            }
                        }

                        function getApps(country) {
                            if (country === undefined)
                                return;
                            
                            const dirname = "data" + path.sep + company;

                            mkdirp(dirname, function (err) {
                                if (err) {
                                    console.error(err)
                                } else {
                                    
                                    const filename = dirname + path.sep + company + "_AndroidApps_" + country.name + ".tsv";
                                    fs.stat(filename, function(err, stats) {
                                        var filemtime, currentTime;
                                        
                                        if (stats != undefined) {
                                            filemtime = Math.round(stats.mtime.getTime()/1000);
                                            currentTime = Math.round(new Date().getTime()/1000);
                                        }
                                            
                                        if( (err == null &&  ( filemtime < (currentTime - (24 * 3600)) ) ) || (err != null && err.code == 'ENOENT')
                                        ) {
                                             console.log("\n\nGetting Google apps for:", country.code + " - " + country.name);

                                            gplay.search({
                                                term: company,
                                                num: 250,
                                                throttle: 1,
                                                country: country.code,
                                                stopEarly: function () {
                                                    function filter(app, term) {
                                                        if (app.developer !== term) {
                                                            console.log('Term:', term, '~', 'Developer:', app.developer,  '~', 'Title:', app.title);
                                                            return false;
                                                        }

                                                        return true;
                                                    }

                                                    return (this.apps.find(app => filter(app, decodeURIComponent(this.term)) === false) !== undefined) ? true : false;
                                                }
                                            }).then(
                                                (apps) => {
                                                    console.log("Number of search results: " + apps.length);

                                                    var gapps = new Map();

                                                    apps.map(function (app) {
                                                        if (app.developer == company) {
                                                            if (!gapps.has(app.appId)) {
                                                                gapps.set(app.appId, { "title": app.title.replace(/"/g, '').replace(/'/g, '').trim(), "description": app.summary.replace(/"/g, '').replace(/'/g, '').trim(), "rating": app.score, "url": app.url, "package": app.appId, "icon": 'https:' + app.icon });
                                                            }
                                                        }
                                                    });

                                                    function sortByProp(data, prop) {
                                                        return [...data.values()].sort((a, b) => a[prop].localeCompare(b[prop]));
                                                    };

                                                    console.log("Number of " + company + " apps in country: " + country.name + " ~ " + gapps.size);

                                                    var csvStream = csv.createWriteStream({headers: true, delimiter: '\t', quoteColumns: true}),
                                                        writableStream = fs.createWriteStream(filename);

                                                    writableStream.on("finish", function(){
                                                        console.log("Finished writing into:", filename);
                                                        completedCountries.push(country.name);
                                                        
                                                        if (fetchNextCountry() == null) {
                                                            process.exit(0);
                                                        }
                                                    });

                                                    csvStream.pipe(writableStream);

                                                    var sortedGapps = sortByProp(gapps, "title");

                                                    sortedGapps.forEach(function (app) {
                                                        csvStream.write(app);
                                                    });

                                                    csvStream.end();

                                                }, function err(error) {
                                                    console.error(error);
                                                    process.exit(1);
                                                }
                                            ); //end then()
                                            
                                        } else {
                                            if (err == null) {
                                                console.log('File is not older than 24 hours,', filename);
                                                fetchNextCountry(true);
                                            }
                                            else {
                                                console.log('Cannot access file,', filename,  'error: ', err);
                                                process.exit();
                                            }
                                        }
                                    }); // end fs stat
                            } // end else
                        }); // end mkdirp
                        } // end getApps
                        
                        fetchNextCountry();
                        
                    }); // end fs end
                }); // end osmosis done
